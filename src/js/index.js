import $ from 'jquery'
window.jQuery = $;
window.$ = $;
import select2 from '../../node_modules/select2/dist/js/select2.min';
/*
ON LOAD PAGE FUNCTION
*/

jQuery( window ).on( 'load', function() {

    $('body').removeClass('is-load');

} );

/*
INITIALIZATION FUNCTIONS
*/

jQuery( document ).ready( function( $ ) {

    let navigationEl = $('nav ul li'),
        formUS       = $('#contactUs');

    $('.js-example-basic-single').select2({
        minimumResultsForSearch: -1
    });

    navigationEl.on('click', function () {
        navigationEl.removeClass('is-active')
        $(this).addClass('is-active')
    })
    function submiting() {
        formUS
            .addClass('is-active')
            .closest('.contacts-content--form')
            .find('.successful-sending').addClass('is-active')
            $('.button-box').remove('preloader')
    }
    formUS.on('submit', function (e) {
        e.preventDefault()
        let errors = validateFormFields(this)
        if(errors) return;
        $('.button-box').addClass('preloader')
        setTimeout(
            submiting,3000);
         })

    $.each(formUS.find('input'), function () {
        $(this).on('input', function (event) {
            validateSingleField(this)
        })
    })
    formUS.find('input , textarea').on('focus',function () {
        formUS.find('label').removeClass('is-active')
        $(this).closest('label').addClass('is-active')
        $('.select2-container').removeClass('is-active')
    })
    $('.select2-selection').on('click', function () {
        formUS.find('label').removeClass('is-active')
        $(this).closest('.select2-container').addClass('is-active')
    })

    let LazyMaps = [].slice.call(document.querySelectorAll(".lazy-map"));
    if ("IntersectionObserver" in window) {
        let lazyMapsObserver = new IntersectionObserver(function (entries, observer) {
            entries.forEach(function (entry) {
                if (entry.isIntersecting) {
                    let LazyMap = entry.target,
                        yaMapScript;

                    if (!document.getElementById('ya-map')) {
                        yaMapScript = document.createElement('script');
                        yaMapScript.setAttribute('src', 'https://api-maps.yandex.ru/2.1/?lang=ru_RU');
                        yaMapScript.setAttribute('id', 'ya-map');
                        document.head.appendChild(yaMapScript);
                    } else {
                        yaMapScript = document.getElementById('ya-map')
                    }

                    if (yaMapScript !== undefined) {
                        yaMapScript.onload = function () {
                            runMapBuild(entry.target);
                        };
                    }

                    lazyMapsObserver.unobserve(LazyMap);
                }
            });
        });

        LazyMaps.forEach(function (LazyMap) {
            lazyMapsObserver.observe(LazyMap);
        });
    }
})



function validateFormFields(form) {
    let _ = $(form),
        error = false;

    $.each(_.find('input'), function (key, el) {
        error = validateSingleField(el);
    });
    return error;
}
function validateSingleField(el) {
    let _el = $(el),
        _ln = _el.val().length,
        _nm = _el.attr('name'),
        _vl = _el.val(),
        error = false,

        _emValPattern = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/,
        _pnValPattern = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{2})?([0-9]{2})$/;
    _el.closest('div').removeClass('error');
    switch (_nm) {
        case 'name':
            if (_ln < 4) {
                _el.closest('div').addClass('error');
                error = true;
            } else {
                _el.closest('div').removeClass('error');
            }
            break;
        case 'email':
            if (!_emValPattern.test(_vl)) {
                _el.closest('div').addClass('error');

                error = true;
            } else {
                _el.closest('div').removeClass('error');
            }
            break;
        case 'phone':
            if (!_pnValPattern.test(_vl) && _ln < 8) {
                _el.closest('div').addClass('error')
                error = true;
            } else {
                _el.closest('div').removeClass('error');
            }
            break;
        default:
            error = false;

    }
    return error;
}


/*
ON SCROLL PAGE FUNCTIONS
*/

jQuery( window ).on( 'scroll', function() {



} );



function runMapBuild(el) {
    ymaps.ready(function () {
        let myMap = new ymaps.Map('myMap', {
                center: [37.776345798800286,-122.39507833264918],
                zoom: 18,
                controls: []
            },
            {
                searchControlProvider: 'yandex#search'
            }),

            myPlacemark = new ymaps.Placemark([37.776818427675884,-122.39809849999997], {
                hintContent: '888 Brannan Street, in San Francisco, California',
                iconContent: '13'
            }, {
                iconLayout: 'default#image',
                iconImageHref: '/img/placemark.svg',
                iconImageSize: [32, 46],
                iconImageOffset: [-24, -24],
                iconContentOffset: [45, 15],
            });


        myMap.geoObjects
            .add(myPlacemark);

        el.classList.remove('lazy-map')
    });
}
